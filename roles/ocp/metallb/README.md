This role installs metallb 0.9.3, largely by using the manifest available from
https://raw.githubusercontent.com/metallb/metallb/v0.9.3/manifests/metallb.yaml.
To install a different version, you'll need to retrieve the updated manifest and
modify as described in the [metallb on OpenShift][] docs.

[metallb on openshift]: https://metallb.universe.tf/installation/clouds/#metallb-on-openshift-ocp
