# MOC CNV Sandbox

Configuration and documentation for the [CNV][] Sandbox at the [Mass Open Cloud][] (MOC).

[cnv]: https://www.redhat.com/en/resources/container-native-virtualization
[mass open cloud]: https://massopen.cloud/

## Playbooks

- `playbook-preinstall.yml`

  Set up provisioning host and generate the install configuration.

- `playbook-postinstall.yml`

  Fetches authentication credentials from the provisioning host and
  then uses the OpenShift API to perform post-configuration tasks
  (installing certificates, configuring SSO, installing CNV, etc).

## See also

- [Getting started with OpenShift and CNV](https://gitlab.com/open-infrastructure-labs/moc-cnv-sandbox/-/tree/docs)

  (Available in the `docs` branch of this repository)
